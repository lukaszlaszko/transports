import select
import errno

from sys import platform
from .transport import Transport
from .signal_handler import SignalHandler

NO_WAIT_TIMEOUT = 0
INFINITE_WAIT_TIMEOUT = -1
DEFAULT_WAIT_TIMEOUT = 1

__EPOLL_PLATFORMS = ['linux', 'linux2']
__KQUEUE_PLATFORMS = ['darwin']


class EpollDispatcher(object):
    """
    Provides dispatch logic for registered transports.
    """
    def __init__(self):
        self.__epoll = select.epoll()
        self.__transports = dict()
        self.__signal_handlers = dict()

    def __del__(self):
        self.__epoll.close()

    def add_signal_handler(self, signum, callback):
        """
        Adds dispatchable signal handler to the dispatcher
        :param signum: The signal to which handler is being registered
        :param callback: A callback to invoke upon reception of the signal
        :return: None
        """
        signal_handler = next((sh for sh in self.__signal_handlers.values() if sh.signum() == signum), None)
        if signal_handler is None:
            signal_handler = SignalHandler(signum)

            self.__signal_handlers[signal_handler.fileno()] = signal_handler
            self.__epoll.register(signal_handler.fileno(), select.EPOLLIN)

        signal_handler.add_callback(callback)

    def register(self, transport, fileno, can_receive, can_send, can_disconnect):
        """
        Registers a transport with the given IO descriptor and dispatch capabilities.
        :param transport: A transport to register
        :param fileno: A corresponding IO descriptor
        :param can_receive: EPOLLIN is allowed
        :param can_send: EPOLLOUT is allowed
        :param can_disconnect: EPOLLHUP is allowed
        :return: None
        """
        assert isinstance(transport, Transport)
        assert isinstance(can_receive, bool)
        assert isinstance(can_send, bool)
        assert isinstance(can_disconnect, bool)

        self.__transports[fileno] = transport

        eventmask = 0
        if can_receive:
            eventmask |= select.EPOLLIN
        if can_send:
            eventmask |= select.EPOLLOUT
        if can_disconnect:
            eventmask |= select.EPOLLHUP

        self.__epoll.register(fileno, eventmask)

    def unregister(self, fileno):
        """
        Unregisters transport related to the given IO descriptor.
        :param fileno: an IO descriptor for which related transport should be removed.
        :return: None
        """
        assert fileno in self.__transports

        del self.__transports[fileno]

    def dispatch(self, timeout=DEFAULT_WAIT_TIMEOUT):
        """
        Waits for an IO event and dispatches it to the corresponding, registered transport.
        :param timeout: IO event wait timeout.
        :return: True if event dispatched, False on timeout
        """
        try:
            events = self.__epoll.poll(timeout)
            if events:
                for fileno, event in events:
                    if fileno in self.__transports:
                        transport = self.__transports[fileno]
                        transport.handle_event((event & select.EPOLLIN) == select.EPOLLIN,
                                               (event & select.EPOLLOUT) == select.EPOLLOUT,
                                               (event & select.EPOLLHUP) == select.EPOLLHUP)
                    elif fileno in self.__signal_handlers and event & select.EPOLLIN:
                        signal_handler = self.__signal_handlers[fileno]
                        signal_handler.invoke()

                return True
            else:
                return False
        except OSError as e:
            if e.errno == errno.EAGAIN:
                return False
            raise


class KQueueDispatcher(object):
    """
    Provides dispatch logic for registered transports over KQueue/KEvent.
    """

    def __init__(self):
        self.__kqueue = select.kqueue()
        self.__kevents = list()
        self.__transports = dict()
        self.__signal_handlers = dict()

    def __del__(self):
        self.__kqueue.close()

    def add_signal_handler(self, signum, callback):
        """
        Adds dispatchable signal handler to the dispatcher
        :param signum: The signal to which handler is being registered
        :param callback: A callback to invoke upon reception of the signal
        :return: None
        """
        signal_handler = next((sh for sh in self.__signal_handlers.values() if sh.signum() == signum), None)
        if signal_handler is None:
            signal_handler = SignalHandler(signum)

            self.__signal_handlers[signal_handler.fileno()] = signal_handler
            kevent = select.kevent(signal_handler.fileno(),
                                   filter=select.KQ_FILTER_READ,
                                   flags=select.KQ_EV_ADD | select.KQ_EV_ENABLE)
            self.__kevents.append(kevent)

        signal_handler.add_callback(callback)

    def register(self, transport, fileno, can_receive, can_send, can_disconnect):
        """
        Registers a transport with the given IO descriptor and dispatch capabilities.
        :param transport: A transport to register
        :param fileno: A corresponding IO descriptor
        :param can_receive: EPOLLIN is allowed
        :param can_send: EPOLLOUT is allowed
        :param can_disconnect: EPOLLHUP is allowed
        :return: None
        """
        assert isinstance(transport, Transport)
        assert isinstance(can_receive, bool)
        assert isinstance(can_send, bool)
        assert isinstance(can_disconnect, bool)

        self.__transports[fileno] = transport

        filter = 0
        if can_receive:
            kevent = select.kevent(fileno,
                                   filter=select.KQ_FILTER_READ,
                                   flags=select.KQ_EV_ADD | select.KQ_EV_ENABLE)
            self.__kevents.append(kevent)
        if can_send:
            kevent = select.kevent(fileno,
                                   filter=select.KQ_FILTER_WRITE,
                                   flags=select.KQ_EV_ADD | select.KQ_EV_ENABLE)
            self.__kevents.append(kevent)

    def unregister(self, fileno):
        """
        Unregisters transport related to the given IO descriptor.
        :param fileno: an IO descriptor for which related transport should be removed.
        :return: None
        """
        assert fileno in self.__transports

        del self.__transports[fileno]
        self.__kevents = [ke for ke in self.__kevents if ke.ident != fileno]

    def dispatch(self, timeout=DEFAULT_WAIT_TIMEOUT):
        """
        Waits for an IO event and dispatches it to the corresponding, registered transport.
        :param timeout: IO event wait timeout.
        :return: True if event dispatched, False on timeout
        """
        try:
            events = self.__kqueue.control(self.__kevents, 1, timeout)
            if events:
                for event in events: # type: select.kevent
                    fileno = event.ident
                    filter = event.filter
                    flags = event.flags
                    if fileno in self.__transports:
                        transport = self.__transports[fileno]
                        transport.handle_event((filter & select.KQ_FILTER_READ) == select.KQ_FILTER_READ,
                                               (filter & select.KQ_FILTER_WRITE) == select.KQ_FILTER_WRITE,
                                               (flags & select.KQ_EV_EOF) == select.KQ_EV_EOF)
                    elif fileno in self.__signal_handlers and filter & select.KQ_FILTER_READ:
                        signal_handler = self.__signal_handlers[fileno]
                        signal_handler.invoke()

                return True
            else:
                return False
        except OSError as e:
            if e.errno == errno.EAGAIN:
                return False
            raise


if platform in __EPOLL_PLATFORMS:
    Dispatcher = EpollDispatcher
elif platform in __KQUEUE_PLATFORMS:
    Dispatcher = KQueueDispatcher
else:
    raise NotImplementedError('Unuspported platform {}!'.format(platform))
import socket
import logging
import struct

from queue import Queue
from .transport import Transport
from .dispatcher import Dispatcher
from .tcp_options import TcpOptions


class ServerTcpTransport(Transport):
    """
    Represents a transport listening for incoming tcp connections at a given address and port. For each client connected
    to the endpoint an individual server-client transport is created.
    """

    class _ConnectedClientTransport(Transport):
        def __init__(self, client_socket, client_address, dispatcher, receive_callback, options):
            super(ServerTcpTransport._ConnectedClientTransport, self).__init__()

            self.__client_socket = client_socket
            self.__client_address = client_address
            self.__dispatcher = dispatcher

            self.__receive_callback = receive_callback
            self.__options = options
            self.__receive_buffer = bytearray(options.read_buffer_size)

            self.open()

        def handle_event(self, should_receive, should_send, should_disconnect):
            if should_receive:
                nbytes = self.__client_socket.recv_into(self.__receive_buffer)
                if nbytes > 0:
                    self.__receive_callback(self, self.__receive_buffer, nbytes)
            if should_send:
                if not self.__scheduled_send_buffer.empty():
                    callback_or_blob = self.__scheduled_send_buffer.get()
                    if isinstance(callback_or_blob, bytearray):
                        self.__client_socket.send(callback_or_blob)
                    else:
                        callback_or_blob(self)
            if should_disconnect:
                self.close()

        def _open(self):
            self.__client_socket_fileno = self.__client_socket.fileno()
            self.__dispatcher.register(self,
                                       self.__client_socket_fileno,
                                       can_receive=self.__receive_callback is not None,
                                       can_send=True,
                                       can_disconnect=True)

            self.__scheduled_send_buffer = Queue()

        def _close(self):
            self.__dispatcher.unregister(self.__client_socket_fileno)
            self.__client_socket.close()

            del self.__scheduled_send_buffer

            logging.debug('tcp server->client transport {} closed.'.format(self.__client_address))

        def send(self, blob):
            """
            Sends blob through the underlying connection.
            :param blob: The blob to send.
            :return:
            """
            assert isinstance(blob, bytearray)
            assert not self.is_closed()

            self.__client_socket.send(blob)

        def schedule_send(self, callback_or_blob):
            """
            Adds th given ``callback_or_blob`` to send queue. If the argument is a callback, the callback will be invoked
            on send dispatch. Once dispatched callback can send data using ``send`` method. If blob is given, buffer will
            be send automatically on the next send dispatch.
            :param callback_or_blob: A callback or blob to send on the next scheduled send event. Signature - ``(transport)``.
            :return: None
            """
            assert callback_or_blob
            assert hasattr(callback_or_blob, '__call__') or isinstance(callback_or_blob, bytearray)
            assert not self.is_closed()

            self.__scheduled_send_buffer.put(callback_or_blob)

        def __repr__(self):
            return repr(self.__client_address)

        def __int__(self):
            return self.__client_socket_fileno

        def __hash__(self):
            return self.__client_socket

        def __eq__(self, other):
            return other is not None and self.__client_socket == other.self.__client_socket

    def __init__(self,
                 dispatcher,
                 address_and_port,
                 client_connected_callback,
                 receive_callback=None,
                 options=TcpOptions()):
        """
        Creates an instance of server tcp transport. The transport listens for incoming connections at the given
        address_and_port. Once an incoming clinet connection is accepted, the transport creates a dependent, unique
        transport for a pair client-server. This newly created transport is then advertised through client_connected_callback.
        :param dispatcher: The dispatcher used to dispatch events for this and all child transports.
        :param address_and_port: An address and port to listen on.
        :param client_connected_callback: A callback invoked once client connection is accepted.
        :param receive_callback: A callback invoked whenever data is received from client.
        :param options: Tcp options.
        """
        assert isinstance(dispatcher, Dispatcher)
        assert isinstance(address_and_port, tuple)
        assert len(address_and_port) == 2
        assert client_connected_callback
        assert hasattr(client_connected_callback, '__call__')
        assert receive_callback is None or hasattr(receive_callback, '__call__')
        assert isinstance(options, TcpOptions)

        super(ServerTcpTransport, self).__init__()

        self.__dispatcher = dispatcher
        self.__address_and_port = address_and_port
        self.__client_connected_callback = client_connected_callback
        self.__receive_callback = receive_callback

        self.__options = options

        self.__client_transports = list()

    def __del__(self):
        self.close()

    def _open(self):
        logging.debug('creating tcp server transport for {}.'.format(self.__address_and_port))

        self.__listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
        self.__listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack('ii', 1, 0))
        self.__listening_socket.bind(self.__address_and_port)
        self.__listening_socket.listen(self.__options.listen_backlog_size)
        self.__listening_socket_fileno = self.__listening_socket.fileno()

        self.__dispatcher.register(self,
                                   self.__listening_socket_fileno,
                                   can_receive=True,
                                   can_send=False,
                                   can_disconnect=False)

        logging.debug('tcp server transport for {} created.'.format(self.__address_and_port))

    def _close(self):
        logging.debug('destroying tcp server transport for {}.'.format(self.__address_and_port))

        self.__dispatcher.unregister(self.__listening_socket_fileno)
        self.__listening_socket.close()

        for client_transport in self.__client_transports:
            client_transport.close()

        logging.debug('tcp server transport for {} destroyed.'.format(self.__address_and_port))

    def handle_event(self, should_receive, should_send, should_disconnect):
        try:
            client_socket, client_address = self.__listening_socket.accept()
            client_socket.setblocking(int(False))
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.__options.recvbuf_size)
            client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, self.__options.sndbuf_size)
            client_socket.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, int(self.__options.no_delay))

            client_transport = ServerTcpTransport._ConnectedClientTransport(client_socket,
                                                                            client_address,
                                                                            self.__dispatcher,
                                                                            self.__receive_callback,
                                                                            self.__options)
            self.__client_connected_callback(client_transport)
        except Exception as ex:
            logging.error('Error when accepting connection on {}'.format(self.__address_and_port), ex)

    def __int__(self):
        return self.__client_socket_fileno

    def __hash__(self):
        return hash(self.__address_and_port)

    def __eq__(self, other):
        if other is None:
            return False
        elif self.__client_socket_fileno != 0:
            return self.__client_socket_fileno == other.__client_socket_fileno
        else:
            return isinstance(other, ServerTcpTransport) and self.__address_and_port == other.__address_and_port
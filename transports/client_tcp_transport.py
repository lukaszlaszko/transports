import socket
import logging
import struct

from queue import Queue
from .transport import Transport
from .dispatcher import Dispatcher
from .tcp_options import TcpOptions


class ClientTcpTransport(Transport):
    """
    A transport representing a tcp connection initiated from a client to listening tcp server.
    """
    __DEFAULT_TIMEOUT = 2

    def __init__(self, dispatcher, address_and_port, connected_callback, receive_callback, options=TcpOptions()):
        """
        Creates an instance of ``ClientTcpTransport`` which is intended to connect to a tcp endpoint given by
        ``address_and_port``. The given ``dispatcher`` is used to dispatch underlying network events.
        :param dispatcher:
        :param address_and_port:
        :param connected_callback: A callback invoked once connection to the remote peer is established. The callback
        has to be a callable object with following signature: ``(transport)`` where ``transport`` will be always the
        related transport. This argument is mandatory.
        :param receive_callback: A callback invoked whenever data is received on the transport. This argument has to be
        a callable object with following signature: ``(transport, buffer, len)`` where ``transport`` is the transport on
         which data has been received, buffer is ``bytearray`` with received data and ``len`` is the actual number of
        received bytes.
        :param options: Tcp options for this transport.
        """

        assert isinstance(dispatcher, Dispatcher)
        assert isinstance(address_and_port, tuple)
        assert len(address_and_port) == 2
        assert connected_callback
        assert hasattr(connected_callback, '__call__')
        assert receive_callback
        assert hasattr(receive_callback, '__call__')
        assert isinstance(options, TcpOptions)

        super(ClientTcpTransport, self).__init__()

        self.__dispatcher = dispatcher
        self.__address_and_port = address_and_port
        self.__connected_callback = connected_callback
        self.__receive_callback = receive_callback

        self.__options = options
        self.__client_socket_fileno = 0

        self.__is_connected = False

    def _open(self):
        logging.debug('opening client tcp transport to {}.'.format(self.__address_and_port))

        self.__is_connected = False

        self.__client_socket = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM, proto=socket.IPPROTO_TCP)
        self.__client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack('ii', 1, 1))
        self.__client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.__options.recvbuf_size)
        self.__client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, self.__options.sndbuf_size)
        self.__client_socket.connect(self.__address_and_port)
        self.__client_socket.setblocking(int(False))
        self.__client_socket_fileno = self.__client_socket.fileno()

        self.__dispatcher.register(self,
                                   self.__client_socket_fileno,
                                   True,
                                   True,
                                   True)

        self.__scheduled_send_buffer = Queue()
        self.__receive_buffer = bytearray(self.__options.read_buffer_size)

        logging.debug('client tcp transport to {} opened.'.format(self.__address_and_port))

    def _close(self):
        logging.debug('closing client tcp transport to {}.'.format(self.__address_and_port))

        self.__is_connected = False

        self.__client_socket.close()
        self.__dispatcher.unregister(self.__client_socket_fileno)

        self.__client_socket_fileno = 0

        del self.__scheduled_send_buffer
        del self.__receive_buffer

        logging.debug('client tcp transport to {} closed.'.format(self.__address_and_port))

    def handle_event(self, should_receive, should_send, should_disconnect):
        if not self.__is_connected:
            self.__is_connected = True
            self.__connected_callback(self)

        if should_receive:
            nbytes = self.__client_socket.recv_into(self.__receive_buffer)
            if nbytes > 0:
                self.__receive_callback(self, self.__receive_buffer, nbytes)
        if should_send:
            if not self.__scheduled_send_buffer.empty():
                callback_or_blob = self.__scheduled_send_buffer.get()
                if isinstance(callback_or_blob, bytearray):
                    self.__client_socket.send(callback_or_blob)
                else:
                    callback_or_blob(self)
        if should_disconnect:
            self.close()

    def send(self, blob):
        """
        Sends raw data directly through the underying socket, without scheduling.
        :param blob: Raw buffer to send.
        :return: None
        """

        assert isinstance(blob, bytearray)
        assert not self.is_closed()

        self.__client_socket.send(blob)

    def schedule_send(self, callback_or_blob):
        """
        Adds th given ``callback_or_blob`` to send queue. If the argument is a callback, the callback will be invoked
        on send dispatch. Once dispatched callback can send data using ``send`` method. If blob is given, buffer will
        be send automatically on the next send dispatch.
        :param callback_or_blob: A callback or blob to send on the next scheduled send event. Signature - ``(transport)``.
        :return: None
        """

        assert callback_or_blob
        assert hasattr(callback_or_blob, '__call__') or isinstance(callback_or_blob, bytearray)
        assert not self.is_closed()

        self.__scheduled_send_buffer.put(callback_or_blob)

    def is_connected(self):
        return self.__is_connected

    def __int__(self):
        return self.__client_socket_fileno

    def __hash__(self):
        return hash(self.__address_and_port)

    def __eq__(self, other):
        if other is None:
            return False
        elif self.__client_socket_fileno != 0:
            return self.__client_socket_fileno == other.__client_socket_fileno
        else:
            return isinstance(other, ClientTcpTransport) and self.__address_and_port == other.__address_and_port

import socket
import struct
import netifaces
import logging

from .transport import Transport
from .dispatcher import Dispatcher
from .udp_options import UdpOptions


class MulticastUdpTransport(Transport):
    """
    Represents a client transport for listening to udp multicast transmission.
    """
    def __init__(self, dispatcher, address_and_port, interface, receive_callback, options=UdpOptions()):
        assert isinstance(dispatcher, Dispatcher)
        assert isinstance(address_and_port, tuple)
        assert len(address_and_port) == 2
        assert receive_callback
        assert hasattr(receive_callback, '__call__')
        assert isinstance(options, UdpOptions)

        super(MulticastUdpTransport, self).__init__()

        self.__dispatcher = dispatcher
        self.__address_and_port = address_and_port
        self.__interface = interface
        self.__receive_callback = receive_callback

        self.__options = options

    def __del__(self):
        self.close()

    def _open(self):
        logging.debug('creating multicast transport for {} on {}'
                      .format(self.__address_and_port, self.__interface))

        self.__multicast_socket = socket.socket(socket.AF_INET, type=socket.SOCK_DGRAM, proto=socket.IPPROTO_UDP)
        self.__multicast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, int(True))
        self.__multicast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, self.__options.recvbuf_size)
        self.__multicast_socket_fileno = self.__multicast_socket.fileno()

        if self.__interface is not None:
            inet_address = (netifaces.ifaddresses(self.__interface)[netifaces.AF_INET])[0]['addr']
            self.__multicast_socket.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(inet_address))

            self.__multicast_socket.bind((self.__address_and_port[0], int(self.__address_and_port[1])))
            self.__multicast_socket.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP,
                                               socket.inet_aton(self.__address_and_port[0]) +
                                               socket.inet_aton(inet_address))
        else:
            self.__multicast_socket.bind((self.__address_and_port[0], int(self.__address_and_port[1])))
            mreq = struct.pack('4sl', socket.inet_aton(self.__address_and_port[0]), socket.INADDR_ANY)
            self.__multicast_socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

        self.__dispatcher.register(self,
                                   self.__multicast_socket_fileno,
                                   can_receive=True,
                                   can_send=False,
                                   can_disconnect=False)

        self.__receive_buffer = bytearray(self.__options.read_buffer_size)

        logging.debug('multicast transport for {} on {} created.'
                      .format(self.__address_and_port, self.__interface))

    def _close(self):
        logging.debug('destroying multicast udp transport for {} on {}.'
                      .format(self.__address_and_port, self.__interface))

        self.__dispatcher.unregister(self.__multicast_socket_fileno)
        self.__multicast_socket.close()

        logging.debug('multicast udp transport for {} on {} destroyed.'
                      .format(self.__address_and_port, self.__interface))

    def handle_event(self, should_receive, should_send, should_disconnect):
        if should_receive:
            nbytes = self.__multicast_socket.recv_into(self.__receive_buffer)
            self.__receive_callback(self.__address_and_port, self.__receive_buffer, nbytes)

    def __int__(self):
        """
        Gets underlying socket identifier
        :return: Underlying socket identifier.
        """
        return self.__client_socket_fileno

    def __hash__(self):
        """
        Gets hash for this transport.
        :return: Transport hash
        """
        return hash(self.__address_and_port)

    def __eq__(self, other):
        if other is None:
            return False
        elif self.__client_socket_fileno != 0:
            return self.__client_socket_fileno == other.__client_socket_fileno
        else:
            return isinstance(other, MulticastUdpTransport) and self.__address_and_port == other.__address_and_port
DEFAULT_LISTEN_BACKLOG_SIZE = 2
DEFAULT_LISTEN_TIMEOUT = 2

DEFAULT_READ_BUFFER_SIZE = 1500
DEFAULT_RECV_BUF_SIZE = 2048
DEFAULT_SND_BUF_SIZE = 2048


class TcpOptions(object):
    """
    Provides transport configuration options for all tcp based transports.
    """

    def __init__(self):
        # listen options
        self.listen_backlog_size = DEFAULT_LISTEN_BACKLOG_SIZE
        self.listen_timeout = DEFAULT_LISTEN_TIMEOUT

        # send receive options
        self.read_buffer_size = DEFAULT_READ_BUFFER_SIZE
        self.recvbuf_size = DEFAULT_RECV_BUF_SIZE
        self.sndbuf_size = DEFAULT_SND_BUF_SIZE

        # tcp level options
        self.no_delay = False
        self.keep_alive = True
import signal
import socket
import logging


class SignalHandler(object):
    __signal_handlers = dict()

    def __init__(self, signum):
        logging.debug('creating signal handler for signum={}'.format(signum))

        self.__signum = signum

        self.__reader, self.__writer = socket.socketpair()
        self.__reader.setblocking(False)
        self.__writer.setblocking(False)

        self.__callbacks = list()

        signum_handlers = SignalHandler.__signal_handlers.setdefault(signum, list())
        if not signum_handlers:
            signal.signal(signum, SignalHandler.__async_handler)
            signal.siginterrupt(signum, False)

        signum_handlers.append(self)

    def __del__(self):
        if self.__signum in SignalHandler.__signal_handlers:
            signum_handlers = SignalHandler.__signal_handlers[self.__signum]
            signum_handlers.remove(self)

        self.__reader.close()
        self.__writer.close()

    def add_callback(self, callback):
        self.__callbacks.append(callback)

    def invoke(self):
        for callback in self.__callbacks:
            callback()

    def signum(self):
        return self.__signum

    def fileno(self):
        return self.__reader.fileno()

    @staticmethod
    def __async_handler(signum, _):
        if signum in SignalHandler.__signal_handlers:
            signum_handlers = SignalHandler.__signal_handlers[signum]
            for signum_handler in signum_handlers:
                signum_handler.__writer.send(bytearray([signum]))

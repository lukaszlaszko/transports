from abc import ABCMeta
from abc import abstractmethod

import threading


class Transport(object):
    """
    Base type for all communication transports.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        self.__closed = True
        self.__owner_thread = None

    @abstractmethod
    def handle_event(self, should_receive, should_send, should_disconnect):
        """
        When implemented in the derived transport, should implement logic
        for handling underlaying IO operation request.
        :param should_receive:
        :param should_send:
        :param should_disconnect:
        :return: None
        """

        pass

    @abstractmethod
    def _open(self):
        pass

    @abstractmethod
    def _close(self):
        pass

    def open(self):
        if self.__closed:
            self._open()
            self.__closed = False
            self.__owner_thread = threading.current_thread()

    def close(self):
        if not self.__closed:
            self._close()
            self.__closed = True

    def is_closed(self):
        return self.__closed

    def __enter__(self):
        self.open()
        return self

    # noinspection PyUnusedLocal
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

from .dispatcher import Dispatcher


class AutoDispatcher(Dispatcher):
    """
    ``AutoDispatcher`` is a derived version of ``Dispatcher`` adding interruptable dispatch loop.
    """

    def __init__(self):
        super(AutoDispatcher, self).__init__()

        self.__is_interrupted = False

    def interrupt(self):
        """
        Interrupts run_forever call.
        :return: None
        """
        self.__is_interrupted = True

    def run_forever(self):
        """
        A loop invoking dispatch until interrupt is called.
        :return: None.
        """
        self.__is_interrupted = False
        while not self.__is_interrupted:
            self.dispatch()

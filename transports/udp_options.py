
DEFAULT_READ_BUFFER_SIZE = 2048
DEFAULT_RCVBUF_SIZE = 1500

class UdpOptions(object):
    def __init__(self):
        self.read_buffer_size = DEFAULT_READ_BUFFER_SIZE
        self.recvbuf_size = DEFAULT_RCVBUF_SIZE
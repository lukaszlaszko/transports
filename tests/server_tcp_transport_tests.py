import unittest
import socket

from hamcrest import *
from transports.dispatcher import Dispatcher
from transports.server_tcp_transport import ServerTcpTransport


_LOCALHOST = '127.0.0.1'
_TEST_TCP_PORT = 10001


class ServerTcpTransportCase(unittest.TestCase):
    def test_connect_disconnect(self):
        dispatcher = Dispatcher()

        class Connection(object):
            def __init__(self):
                self.connection_made = False
                self.client_transport = None

            def on_connected(self, client_transport):
                self.connection_made = True
                self.client_transport = client_transport

        connection = Connection()
        with ServerTcpTransport(dispatcher, (_LOCALHOST, _TEST_TCP_PORT), connection.on_connected) as transport:
            # connect tcp client
            s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM, proto=socket.IPPROTO_TCP)
            s.settimeout(2.0)
            s.connect((_LOCALHOST, _TEST_TCP_PORT))

            # dispatch socket accept
            dispatcher.dispatch(10)

            # validation connection made
            assert_that(connection.connection_made)
            assert_that(connection.client_transport, is_not(None))
            assert_that(connection.client_transport.is_closed(), is_(False))

            # disconnect tcp client
            s.close()
            dispatcher.dispatch()
            dispatcher.dispatch()

            # validate disconnection
            assert_that(connection.client_transport, is_not(None))
            assert_that(connection.client_transport.is_closed(), is_(False))

            # close client socket
            s.close()

    def test_schedule_send_blob(self):
        dispatcher = Dispatcher()

        class Connection(object):
            def __init__(self):
                self.connection_made = False
                self.client_transport = None

            def on_connected(self, client_transport):
                self.connection_made = True
                self.client_transport = client_transport

        connection = Connection()
        with ServerTcpTransport(dispatcher, (_LOCALHOST, _TEST_TCP_PORT + 1), connection.on_connected) as transport:
            # connect tcp client
            s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM, proto=socket.IPPROTO_TCP)
            s.settimeout(2.0)
            s.connect((_LOCALHOST, _TEST_TCP_PORT + 1))

            # dispatch socket accept
            dispatcher.dispatch(10)

            # validation connection made
            assert_that(connection.connection_made)
            assert_that(connection.client_transport, is_not(None))

            # send internal buffer data
            send_data = bytearray(range(0, 6))
            connection.client_transport.schedule_send(send_data)

            # send data to client through send dispatch
            dispatcher.dispatch()

            # receive on tcp client
            received_data = s.recv(1024)
            received_data = bytearray(received_data)

            assert_that(received_data, is_not(None))

            connection.client_transport.close()
            assert_that(connection.client_transport.is_closed())

            # close client socket
            s.close()

    def test_schedule_send_callback(self):
        dispatcher = Dispatcher()

        class Connection(object):
            def __init__(self):
                self.connection_made = False
                self.client_transport = None

            def on_connected(self, client_transport):
                self.connection_made = True
                self.client_transport = client_transport

        connection = Connection()
        with ServerTcpTransport(dispatcher, (_LOCALHOST, _TEST_TCP_PORT + 2), connection.on_connected) as transport:
            # connect tcp client
            s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM, proto=socket.IPPROTO_TCP)
            s.settimeout(2.0)
            s.connect((_LOCALHOST, _TEST_TCP_PORT + 2))

            # dispatch socket accept
            dispatcher.dispatch(10)

            # send internal buffer data
            def scheduled_send(transport):
                send_data = bytearray(range(0, 6))
                transport.send(send_data)
            connection.client_transport.schedule_send(scheduled_send)

            # send data to client through send dispatch
            dispatcher.dispatch()

            # receive on tcp client
            received_data = s.recv(1024)
            received_data = bytearray(received_data)

            assert_that(received_data, is_not(None))

            connection.client_transport.close()
            assert_that(connection.client_transport.is_closed())

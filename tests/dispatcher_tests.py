import unittest
import signal
import os

from hamcrest import *
from transports.dispatcher import Dispatcher


class DispatcherCase(unittest.TestCase):
    def test_add_signal_handler(self):
        dispatcher = Dispatcher()

        class Handler(object):
            def __init__(self):
                self.signal_handled = False

            def on_signal(self):
                self.signal_handled = True

        handler = Handler()
        dispatcher.add_signal_handler(signal.SIGUSR1, handler.on_signal)
        assert_that(handler.signal_handled is False)

        os.kill(os.getpid(), signal.SIGUSR1)
        assert_that(handler.signal_handled is False)

        dispatcher.dispatch()
        assert_that(handler.signal_handled is True)

    def test_add_signal_handler_2_signals(self):
        dispatcher = Dispatcher()

        class Handler(object):
            def __init__(self):
                self.signal_handled = False

            def on_signal(self):
                self.signal_handled = True

        handler = Handler()
        dispatcher.add_signal_handler(signal.SIGUSR1, handler.on_signal)
        dispatcher.add_signal_handler(signal.SIGUSR2, handler.on_signal)
        assert_that(handler.signal_handled is False)

        os.kill(os.getpid(), signal.SIGUSR1)
        assert_that(handler.signal_handled is False)

        dispatcher.dispatch()
        assert_that(handler.signal_handled is True)

    def test_add_signal_handler_2_dispatchers(self):
        dispatcher1 = Dispatcher()
        dispatcher2 = Dispatcher()

        class Handler(object):
            def __init__(self):
                self.signal_handled = False

            def on_signal(self):
                self.signal_handled = True

        handler1 = Handler()
        dispatcher1.add_signal_handler(signal.SIGUSR1, handler1.on_signal)
        assert_that(handler1.signal_handled is False)

        handler2 = Handler()
        dispatcher2.add_signal_handler(signal.SIGUSR1, handler2.on_signal)
        assert_that(handler2.signal_handled is False)

        os.kill(os.getpid(), signal.SIGUSR1)
        assert_that(handler1.signal_handled is False)
        assert_that(handler2.signal_handled is False)

        dispatcher1.dispatch()
        assert_that(handler1.signal_handled is True)
        assert_that(handler2.signal_handled is False)

        dispatcher2.dispatch()
        assert_that(handler2.signal_handled is True)
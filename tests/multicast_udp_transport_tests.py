import unittest
import socket

from hamcrest import *
from transports.dispatcher import Dispatcher
from transports.multicast_udp_transport import MulticastUdpTransport

_MULTICAST_GROUP_1 = '224.0.56.56'
_MULTICAST_GROUP_2 = '224.0.56.57'
_MULTICAST_PORT_1 = 34567
_MULTICAST_PORT_2 = 34568

_MULTICAST_ADDRESS_AND_PORT_1 = (_MULTICAST_GROUP_1, _MULTICAST_PORT_1)
_MULTICAST_ADDRESS_AND_PORT_2 = (_MULTICAST_GROUP_2, _MULTICAST_PORT_2)


class MulticastUdpTransportCase(unittest.TestCase):
    def test_create(self):
        dispatcher = Dispatcher()

        class Receiver(object):
            def on_received(self, source, blob, len):
                pass

        receiver = Receiver()
        transport = MulticastUdpTransport(dispatcher, _MULTICAST_ADDRESS_AND_PORT_1, None, receiver.on_received)

    def test_receive_from_single_group(self):
        dispatcher = Dispatcher()

        class Receiver(object):
            def __init__(self):
                self.received_data = dict()

            def on_received(self, source, blob, len):
                if not source in self.received_data:
                    self.received_data[source] = list()
                self.received_data[source].append(blob)

        receiver = Receiver()
        with MulticastUdpTransport(dispatcher, _MULTICAST_ADDRESS_AND_PORT_1, None, receiver.on_received) as transport:
            # test publisher
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 0)

            # send test data
            send_buffer = bytearray([1, 2, 3, 4])
            s.sendto(send_buffer, _MULTICAST_ADDRESS_AND_PORT_1)

            # dispatch socket receive
            dispatcher.dispatch(5)

            assert_that(receiver.received_data, is_not(empty()))
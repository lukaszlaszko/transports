import unittest
import time

from hamcrest import *
from transports.dispatcher import Dispatcher
from transports.server_tcp_transport import ServerTcpTransport
from transports.client_tcp_transport import ClientTcpTransport

_LOCALHOST = '127.0.0.1'
_TEST_TCP_PORT = 11001


class ClientTcpTransportCase(unittest.TestCase):
    def test_connect_disconnect(self):
        # dispatchers
        server_dispatcher = Dispatcher()
        client_dispatcher = Dispatcher()

        # server
        class ServerMock(object):
            def __init__(self):
                self.client_connected = False
                self.client_transport = None

            def on_client_connected(self, transport):
                self.client_connected = True
                self.client_transport = transport

        server = ServerMock()
        with ServerTcpTransport(server_dispatcher, (_LOCALHOST, _TEST_TCP_PORT), server.on_client_connected) as server_transport:
            # client
            class ClientMock(object):
                def __init__(self):
                    self.connected_to_server = False

                def on_connected(self, transport):
                    self.connected_to_server = True

                def on_received(self, transport, blob, len):
                    pass

            client = ClientMock()
            with ClientTcpTransport(client_dispatcher, (_LOCALHOST, _TEST_TCP_PORT), client.on_connected, client.on_received) as client_transport:
                server_dispatcher.dispatch()
                assert_that(server.client_connected is True)

                client_dispatcher.dispatch()
                assert_that(client.connected_to_server is True)

                assert_that(server.client_transport, is_not(None))
                server.client_transport.close()

                server_dispatcher.dispatch()

                # client disconnected
                assert_that(server.client_transport.is_closed() is True)

    def test_send_client_to_server(self):
        # dispatchers
        server_dispatcher = Dispatcher()
        client_dispatcher = Dispatcher()

        # server
        class ServerMock(object):
            def __init__(self):
                self.client_connected = False
                self.received_data = list()

            def on_client_connected(self, transport):
                self.client_connected = True

            def on_received(self, transport, blob, len):
                self.received_data.append((repr(transport), blob[0:len], len))

        server = ServerMock()
        with ServerTcpTransport(server_dispatcher, (_LOCALHOST, _TEST_TCP_PORT + 1), server.on_client_connected, server.on_received) as server_transport:
            # client
            class ClientMock(object):
                def __init__(self):
                    self.connected_to_server = False

                def on_connected(self, transport):
                    self.connected_to_server = True

                def on_received(self, transport, blob, len):
                    pass

            client = ClientMock()
            with ClientTcpTransport(client_dispatcher, (_LOCALHOST, _TEST_TCP_PORT + 1), client.on_connected,
                                    client.on_received) as client_transport:
                server_dispatcher.dispatch()
                assert_that(server.client_connected is True)

                client_dispatcher.dispatch()
                assert_that(client.connected_to_server is True)

                # prepare buffer and send async
                send_data = bytearray(range(0, 6))
                client_transport.schedule_send(send_data)
                assert_that(server.received_data, is_(empty()))

                # dispatch send from client
                client_dispatcher.dispatch()

                time.sleep(0.5)

                # dispatch server reception
                server_dispatcher.dispatch()
                assert_that(server.received_data, is_not(empty()))
                assert_that(server.received_data, has_length(1))
                assert_that(server.received_data[0][1], is_(instance_of(bytearray)))
                assert_that(server.received_data[0][1], has_length(len(send_data)))

    def test_send_server_to_client(self):
        # dispatchers
        server_dispatcher = Dispatcher()
        client_dispatcher = Dispatcher()

        # server
        class ServerMock(object):
            def __init__(self):
                self.client_connected = False
                self.send_data = bytearray(range(0, 6))

            def on_client_connected(self, transport):
                self.client_connected = True
                transport.schedule_send(self.send_data)

        server = ServerMock()
        with ServerTcpTransport(server_dispatcher, (_LOCALHOST, _TEST_TCP_PORT + 2), server.on_client_connected) as server_transport:
            # client
            class ClientMock(object):
                def __init__(self):
                    self.connected_to_server = False
                    self.received_data = list()

                def on_connected(self, transport):
                    self.connected_to_server = True

                def on_received(self, transport, blob, len):
                    self.received_data.append((transport, blob[0:len], len))

            client = ClientMock()
            with ClientTcpTransport(client_dispatcher, (_LOCALHOST, _TEST_TCP_PORT + 2), client.on_connected,
                                    client.on_received) as client_transport:
                # dispatch connect on server side
                server_dispatcher.dispatch()
                assert_that(server.client_connected is True)
                assert_that(client.received_data, is_(empty()))

                # dispatch send from server
                server_dispatcher.dispatch()

                time.sleep(0.5)

                # dispatch connect and receive on client side
                client_dispatcher.dispatch()
                assert_that(client.received_data, is_not(empty()))
                assert_that(client.received_data, has_length(1))
                assert_that(client.received_data[0][1], is_(instance_of(bytearray)))
                assert_that(client.received_data[0][1], has_length(len(server.send_data)))


